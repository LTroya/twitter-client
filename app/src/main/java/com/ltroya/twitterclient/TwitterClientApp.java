package com.ltroya.twitterclient;

import android.app.Application;
import android.support.v4.app.Fragment;

import com.ltroya.twitterclient.hashtags.di.DaggerHashtagComponent;
import com.ltroya.twitterclient.hashtags.di.HashtagComponent;
import com.ltroya.twitterclient.hashtags.di.HashtagModule;
import com.ltroya.twitterclient.hashtags.ui.HashtagsView;
import com.ltroya.twitterclient.images.di.DaggerImagesComponent;
import com.ltroya.twitterclient.images.di.ImagesComponent;
import com.ltroya.twitterclient.images.di.ImagesModule;
import com.ltroya.twitterclient.images.ui.ImagesView;
import com.ltroya.twitterclient.lib.di.LibsModule;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

public class TwitterClientApp extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        initFabric();
    }

    private void initFabric() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(BuildConfig.TWITTER_KEY, BuildConfig.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
    }

    public ImagesComponent getImagesComponent(Fragment fragment, ImagesView view, com.ltroya.twitterclient.images.ui.adapters.OnItemClickListener clickListener) {
        return DaggerImagesComponent
                .builder()
                .libsModule(new LibsModule(fragment))
                .imagesModule(new ImagesModule(view, clickListener))
                .build();
    }

    public HashtagComponent getHashTagsComponent(HashtagsView view, com.ltroya.twitterclient.hashtags.ui.adapters.OnItemClickListener clickListener) {
        return DaggerHashtagComponent
                .builder()
                .libsModule(new LibsModule(null))
                .twitterAppModule(new TwitterAppModule(getApplicationContext()))
                .hashtagModule(new HashtagModule(view, clickListener))
                .build();
    }
}
