package com.ltroya.twitterclient.hashtags.di;

import android.content.Context;

import com.ltroya.twitterclient.api.CustomTwitterApiClient;
import com.ltroya.twitterclient.entities.Hashtag;
import com.ltroya.twitterclient.hashtags.HashtagsInteractor;
import com.ltroya.twitterclient.hashtags.HashtagsInteractorImpl;
import com.ltroya.twitterclient.hashtags.HashtagsPresenter;
import com.ltroya.twitterclient.hashtags.HashtagsPresenterImpl;
import com.ltroya.twitterclient.hashtags.HashtagsRepository;
import com.ltroya.twitterclient.hashtags.HashtagsRepositoryImpl;
import com.ltroya.twitterclient.hashtags.ui.HashtagsView;
import com.ltroya.twitterclient.hashtags.ui.adapters.HashtagsAdapter;
import com.ltroya.twitterclient.hashtags.ui.adapters.OnItemClickListener;
import com.ltroya.twitterclient.lib.base.EventBus;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class HashtagModule {
    private HashtagsView view;
    private OnItemClickListener clickListener;

    public HashtagModule(HashtagsView view, OnItemClickListener clickListener) {
        this.view = view;
        this.clickListener = clickListener;
    }

    @Provides
    @Singleton
    List<Hashtag> provideItems() {
        return new ArrayList<Hashtag>();
    }

    @Provides
    @Singleton
    OnItemClickListener providesClickListener() {
        return this.clickListener;
    }

    @Provides
    HashtagsAdapter providesAdapter(Context context, List<Hashtag> items, OnItemClickListener clickListener) {
        return new HashtagsAdapter(context, items, clickListener);
    }

    @Provides
    @Singleton
    HashtagsView providesHashTagsView() {
        return this.view;
    }

    @Provides
    @Singleton
    HashtagsPresenter providesHashtagsPresenter(HashtagsView view, HashtagsInteractor interactor, EventBus eventBus) {
        return new HashtagsPresenterImpl(view, interactor, eventBus);
    }

    @Provides
    @Singleton
    HashtagsInteractor providesHashtagsInteractor(HashtagsRepository repository) {
        return new HashtagsInteractorImpl(repository);
    }

    @Provides
    @Singleton
    HashtagsRepository providesHashtagsRepository(CustomTwitterApiClient client, EventBus eventBus) {
        return new HashtagsRepositoryImpl(client, eventBus);
    }

    @Provides
    @Singleton
    CustomTwitterApiClient providesTwitterApiClient(TwitterSession session) {
        return new CustomTwitterApiClient(session);
    }

    @Provides
    @Singleton
    TwitterSession provideTwitterSession() {
        return Twitter.getSessionManager().getActiveSession();
    }
}
