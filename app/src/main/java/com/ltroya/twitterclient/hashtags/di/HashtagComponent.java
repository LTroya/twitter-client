package com.ltroya.twitterclient.hashtags.di;

import com.ltroya.twitterclient.TwitterAppModule;
import com.ltroya.twitterclient.hashtags.ui.HashtagsFragment;
import com.ltroya.twitterclient.lib.di.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, HashtagModule.class, TwitterAppModule.class})
public interface HashtagComponent {
    void inject(HashtagsFragment fragment);
}
