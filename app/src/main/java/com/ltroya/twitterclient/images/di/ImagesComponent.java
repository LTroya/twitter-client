package com.ltroya.twitterclient.images.di;

import com.ltroya.twitterclient.images.ImagesPresenter;
import com.ltroya.twitterclient.images.ui.ImagesFragment;
import com.ltroya.twitterclient.lib.di.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, ImagesModule.class})

public interface ImagesComponent {
    void inject(ImagesFragment fragment);
    ImagesPresenter getPresenter();
}
