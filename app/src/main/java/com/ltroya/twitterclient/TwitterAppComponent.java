package com.ltroya.twitterclient;

import javax.inject.Singleton;

import dagger.Component;

@Singleton @Component(modules = {TwitterAppModule.class})
public interface TwitterAppComponent {
}
