package com.ltroya.twitterclient.hashtags;

import android.util.Log;

import com.ltroya.twitterclient.api.CustomTwitterApiClient;
import com.ltroya.twitterclient.entities.Hashtag;
import com.ltroya.twitterclient.entities.Image;
import com.ltroya.twitterclient.hashtags.events.HashtagEvent;
import com.ltroya.twitterclient.images.events.ImagesEvent;
import com.ltroya.twitterclient.lib.base.EventBus;
import com.twitter.sdk.android.core.models.HashtagEntity;
import com.twitter.sdk.android.core.models.MediaEntity;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HashtagsRepositoryImpl implements HashtagsRepository{
    private CustomTwitterApiClient client;
    private EventBus eventBus;
    private final static int TWEET_COUNT = 100;

    public HashtagsRepositoryImpl(CustomTwitterApiClient client, EventBus eventBus) {
        this.client = client;
        this.eventBus = eventBus;
    }

    @Override
    public void getHashtags() {
        Callback<List<Tweet>> callback = new Callback<List<Tweet>>() {
            @Override
            public void success(List<Tweet> tweets, Response response) {
                List<Hashtag> items = new ArrayList<>();
                for (Tweet tweet : tweets) {
                    if (containsHashtags(tweet)) {
                        Hashtag tweetModel = new Hashtag();

                        tweetModel.setId(tweet.idStr);
                        tweetModel.setFavoriteCount(tweet.favoriteCount);
                        tweetModel.setTweetText(tweet.text);

                        List<String> hashtags = new ArrayList<>();
                        for (HashtagEntity hashtag : tweet.entities.hashtags) {
                            hashtags.add(hashtag.text);
                        }
                        tweetModel.setHashtags(hashtags);
                        items.add(tweetModel);
                    }
                }

                Collections.sort(items, new Comparator<Hashtag>() {
                    @Override
                    public int compare(Hashtag image, Hashtag t1) {
                        return t1.getFavoriteCount() - image.getFavoriteCount();
                    }
                });

                post(items);
            }

            @Override
            public void failure(RetrofitError error) {
                post(error.getLocalizedMessage());
            }
        };
        client.getTimelineService().homeTimeline(TWEET_COUNT, true, true, true, true, callback);
    }

    private boolean containsHashtags(Tweet tweet) {
        return tweet.entities != null
                && tweet.entities.hashtags != null
                && ! tweet.entities.hashtags.isEmpty();
    }

    private void post(List<Hashtag> items) {
        post(items, null);
    }

    private void post(String error) {
        post(null, error);
    }

    private void post(List<Hashtag> items, String error) {
        HashtagEvent event = new HashtagEvent();
        event.setError(error);
        event.setHashtags(items);
        eventBus.post(event);
    }
}
