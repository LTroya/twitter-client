package com.ltroya.twitterclient.hashtags.ui;

import com.ltroya.twitterclient.entities.Hashtag;

import java.util.List;

public interface HashtagsView {
    void showList();
    void hideList();
    void showProgress();
    void hideProgress();

    void onHashtagsError(String error);
    void setHashtags(List<Hashtag> items);
}
