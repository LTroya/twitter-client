package com.ltroya.twitterclient.hashtags;

public interface HashtagsRepository {
    void getHashtags();
}
