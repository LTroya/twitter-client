package com.ltroya.twitterclient.images.ui.adapters;

import com.ltroya.twitterclient.entities.Image;

public interface OnItemClickListener {
    void onItemClick(Image image);
}
