package com.ltroya.twitterclient.hashtags;

import com.ltroya.twitterclient.entities.Hashtag;
import com.ltroya.twitterclient.hashtags.events.HashtagEvent;
import com.ltroya.twitterclient.hashtags.ui.HashtagsView;
import com.ltroya.twitterclient.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class HashtagsPresenterImpl implements HashtagsPresenter{
    private HashtagsView hashtagsView;
    private HashtagsInteractor hashtagsInteractor;
    private EventBus eventBus;

    public HashtagsPresenterImpl(HashtagsView hashtagsView, HashtagsInteractor hashtagsInteractor, EventBus eventBus) {
        this.hashtagsView = hashtagsView;
        this.hashtagsInteractor = hashtagsInteractor;
        this.eventBus = eventBus;
    }

    @Override
    public void onResume() {
        eventBus.register(this);
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
    }

    @Override
    public void onDestroy() {
        hashtagsView = null;
    }

    @Override
    public void getHashtagTweets() {
        if (this.hashtagsView != null) {
            hashtagsView.hideList();
            hashtagsView.showProgress();
        }
        this.hashtagsInteractor.getHashtagItemsList();
    }

    @Override
    @Subscribe
    public void onEventMainThread(HashtagEvent event) {
        String errorMsg = event.getError();
        if (this.hashtagsView != null) {
            hashtagsView.showList();
            hashtagsView.hideProgress();
            if (errorMsg != null) {
                this.hashtagsView.onHashtagsError(errorMsg);
            } else {
                List<Hashtag> items=  event.getHashtags();
                if (items != null && !items.isEmpty()) {
                    this.hashtagsView.setHashtags(items);
                }
            }
        }
    }
}
