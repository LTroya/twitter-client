package com.ltroya.twitterclient.hashtags.ui.adapters;

import com.ltroya.twitterclient.entities.Hashtag;

public interface OnItemClickListener {
    void onItemClick(Hashtag tweet);
}
