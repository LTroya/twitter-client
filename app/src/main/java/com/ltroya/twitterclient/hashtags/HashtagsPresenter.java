package com.ltroya.twitterclient.hashtags;

import com.ltroya.twitterclient.hashtags.events.HashtagEvent;

public interface HashtagsPresenter {
    void onResume();
    void onPause();
    void onDestroy();
    void getHashtagTweets();
    void onEventMainThread(HashtagEvent event);
}
