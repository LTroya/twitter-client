package com.ltroya.twitterclient.hashtags;

public interface HashtagsInteractor {
    void getHashtagItemsList();
}
