package com.ltroya.twitterclient.hashtags.events;

import com.ltroya.twitterclient.entities.Hashtag;

import java.util.List;

public class HashtagEvent {
    private String error;
    private List<Hashtag> hashtags;

    public List<Hashtag> getHashtags() {
        return hashtags;
    }

    public void setHashtags(List<Hashtag> hashtags) {
        this.hashtags = hashtags;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
